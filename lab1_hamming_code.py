import bitarray as ba
import numpy as np
from hamming_code_abstract import HammingCodeAbstract
from typing import List, Optional, Tuple, Union


class HammingCode(HammingCodeAbstract):
    def __init__(self):
        super().__init__()
        pass

    def from_string(self, array_str: str) -> Union[ba.bitarray, np.array]:
        pass

    def to_string(self, arr: Union[ba.bitarray, np.array]) -> str:
        pass

    @staticmethod
    def bitarray_to_numpy_array(arr: ba.bitarray) -> np.array:
        pass

    @staticmethod
    def numpy_array_to_bitarray(arr: np.array) -> ba.bitarray:
        pass

    def encode(self, arr: Union[ba.bitarray, np.array]) -> Union[ba.bitarray, np.array]:
        pass

    def decode(self, arr: Union[ba.bitarray, np.array]) -> Union[ba.bitarray, np.array]:
        pass

    def get_error_index(self, parity_bits: Union[ba.bitarray, np.array]):
        pass

    def read_file(self, input_path: str) -> str:
        pass

    def read_bits_from_file(self, input_path: str) -> Optional[Union[ba.bitarray, np.array]]:
        pass

    def write_bits(self, bits: Union[ba.bitarray, np.array], ouput_path: str) -> None:
        pass

    def encode_file(self, input_file_path: str, encoded_file_path: str):
        pass

    def make_errors_in_encoded_file(self, encoded_file_path: str, faulty_file_path: str, faulty_index_list: List[int]):
        pass

    def decode_file(self, encoded_file_path: str, decoded_file_path: str) -> List[int]:
        pass


def task_1():
    hc = HammingCode()


def main():
    pass


if __name__ == "__main__":
    main()
