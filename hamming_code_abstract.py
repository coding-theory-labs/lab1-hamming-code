
import bitarray as ba
import enum
import numpy as np
from abc import ABC, abstractmethod
from typing import List, Optional, Tuple, Union


class HammingCodeAbstract(ABC):
    """
    Абстрактный класс кодов Хэмминга
    """
    @abstractmethod
    def __init__(self):
        """Конструктор"""
        pass

    @abstractmethod
    def from_string(self, array_str: str) -> Union[ba.bitarray, np.array]:
        """
        Преобразование строки в битовую последовательность

        :param array_str: строковое представление битовой последовательности
        :returns: битовая последовательность
        """
        pass

    @abstractmethod
    def to_string(self, arr: Union[ba.bitarray, np.array]) -> str:
        """
        Преобразование битовой последовательности в строку

        :param arr: битовая последовательность
        :returns: строковое представление битовой последовательности
        """
        pass

    @staticmethod
    @abstractmethod
    def bitarray_to_numpy_array(arr: ba.bitarray) -> np.array:
        """
        Преобразование битовой последовательности в массив numpy

        :param arr: битовая последовательность
        :returns: массив numpy
        """
        pass

    @staticmethod
    @abstractmethod
    def numpy_array_to_bitarray(arr: np.array) -> ba.bitarray:
        """
        Преобразование массива numpy в битовую последовательность

        :param arr: массив numpy
        :returns: битовая последовательность
        """
        pass

    @abstractmethod
    def encode(self, arr: Union[ba.bitarray, np.array]) -> Union[ba.bitarray, np.array]:
        """
        Кодирование сообщения

        :param arr: исходное сообщение
        :returns: закодированное сообщение
        """
        pass

    @abstractmethod
    def get_error_index(self, parity_bits: Union[ba.bitarray, np.array]):
        """
        Индекс ошибки закодированного сообщения

        :param parity_bits: код ошибки
        :returns: индекс ошибки
        """
        pass

    @abstractmethod
    def decode(self, arr: Union[ba.bitarray, np.array]) -> Tuple[Union[ba.bitarray, np.array], int]:
        """
        Декодирование сообщения

        :param arr: исходное сообщение
        :returns: декодированное сообщение и индекс ошибки
        """
        pass

    def read_file(self, input_path: str) -> str:
        """
        Чтение содержимого файла

        :param input_path: путь к текстовому файлу
        :returns: текст
        """
        pass

    def read_bits_from_file(self, input_path: str) -> Optional[Union[ba.bitarray, np.array]]:
        """
        Чтение содержимого файла в битовую последовательность

        :param input_path: путь к текстовому файлу
        :returns: битовая последовательность
        """
        pass

    def write_bits(self, bits: Union[ba.bitarray, np.array], ouput_path: str) -> None:
        """
        Запись битовой последовательности в файл

        :param bits: битовая последовательность
        :param ouput_path: путь к текстовому файлу
        """
        pass

    @abstractmethod
    def encode_file(self, input_file_path: str, encoded_file_path: str):
        """
        Кодирование файла на диске

        :param input_file_path: путь к исходному файлу на диске
        :param encoded_file_path: путь к закодированному файлу на диске
        """
        pass

    @abstractmethod
    def make_errors_in_encoded_file(self, encoded_file_path: str, faulty_file_path: str, faulty_index_list: List[int]):
        """
        Внесение в закодированный файл ошибок

        :param encoded_file_path: путь к закодированному файлу на диске
        :param faulty_file_path: путь к закодированному файлу на диске с внесенными ошибками
        :param faulty_index_list: массив индексов ошибок
        """
        pass

    @abstractmethod
    def decode_file(self, encoded_file_path: str, decoded_file_path: str) -> List[int]:
        """
        Декодирование файла на диске с обнаружением и исправлением ошибок

        :param encoded_file_path: путь к закодированному файлу на диске
        :param decoded_file_path: путь к декодированному файлу на диске
        :returns: массив индексов ошибок
        """
        pass
